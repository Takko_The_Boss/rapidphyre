import paho.mqtt.client as mqtt #import the client1
import time
#from mqtt_list import final
############
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)
########################################
broker_address=str(input("Please enter MQTT Target: "))
#broker_address="iot.eclipse.org"
print("creating new instance")
client = mqtt.Client("P1") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(broker_address) #connect to broker
client.loop_start() #start the loop
sub_topic=input("Please enter topic to subscribe to: ")
pub_topic=input("Please enter topic to publish to: ")
pub_message=input("Please enter message to publish: ")
print("Subscribing to topic",sub_topic)
client.subscribe(sub_topic)
print("Publishing message to topic",pub_topic)
client.publish("sensors/sensor-3542/control",pub_message)
time.sleep(4) # wait
client.loop_stop() #stop the loop