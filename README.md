# RapidPhyre

RapidPhyre is a framework of OT pentesting tools. It is designed to simplify the process of OT pentesting.

This was built initially to help rapidly send messages to ModBus enabled components for fuzz testing. It's since been built to include other technologies as well.

## Dependencies:

### python3
- halo
- paho-mqtt