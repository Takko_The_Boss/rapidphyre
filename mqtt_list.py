import paho.mqtt.client as mqtt
import time
import os



def on_connect(client, userdata, rc, flags):
	client.subscribe('#', qos=1)
	client.subscribe('$SYS/#')

def on_message(client, userdata, message):
	print('Topic: %s | QOS: %s  | Message: %s' % (message.topic, message.qos, message.payload))

def main():
	final = str(input("Please enter MQTT Target: "))
	print("IP: %s" %final)
	client = mqtt.Client("Instance")
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect(final)
	client.loop_start()
	time.sleep(10)
	client.loop_stop()

if __name__ == "__main__":
	#search()
	main()
